# pdfcheck #

The script to check PDF files. It can be used not only to see PDF file info but it is capable to verify digital signatures and to check certificates with OpenSSL. 

### Install ###

To use this script you have to build it and install. In the current directory run:

```sh
make
```

By default the script will be installed to **/usr/local/bin/pdfcheck**. Man page is under **/usr/local/share/man/man1**. To get info run:
```sh
man pdfcheck
```

**Note**: OpenSSL package should be installed on the system.

### Examples ###

Get help info:
```sh
$ pdfcheck -h
Usage: /usr/local/bin/pdfcheck options

Options are:
 --in source      Input PDF file
 --sign file      Save signature to file in DER format
 --cert file      Save certificate to file in PEM format
 --ca source      A source of trusted certificates. Used only with --crl option
 --crl source     Source of CRL's (in PEM format). Used only with --ca option
 -i               Get PDF info
 -s               Find signature
 -c               Get certificate info
 -q               Quiet mode 
 -h               This help info

Returned codes:

0 - OK
1 - usage error
2 - signature was not found
3 - signature is wrong
4 - certificate is wrong

In "quiet mode" you can check return code by running:

> echo $?

```

Show PDF info, find and check signature and get certificate info:
```sh
$ pdfcheck --in pdfSigned.pdf -isc 
```

Find and check signature and verify certificate:
```sh
$ pdfcheck --in pdfSigned.pdf -s --ca ca-chain.cert.pem --crl intermediate.crl.pem
```

Check signature and verify certificate in quiet mode:
```sh
$ pdfcheck --in pdfSigned.pdf -q --ca ca-chain.cert.pem --crl intermediate.crl.pem
$ echo $?
```
