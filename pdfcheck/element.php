<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Zabara <wlady2001@gmail.com>
 * Date: 11/23/17
 * Time: 12:31 PM
 */

namespace PDFReader;


class Element
{
    protected $document = null;
    protected $engine = null;
    protected $fp = null;
    protected $number = 0;
    protected $start = 0;
    protected $revision = 0;
    protected $data = null;
    protected $tempSignName = '';
    protected $tempCertName = '';

    public function __construct($document, $start, $revision, $number)
    {
        $this->document = $document;
        $this->engine = $document->engine;
        $this->fp = $document->fp;
        $this->start = $start;
        $this->revision = $revision;
        $this->number = $number;
    }

    public function __destruct()
    {
        @unlink($this->tempSignName);
        @unlink($this->tempCertName);
    }

    /**
     * Detect if current element is a signature of type PKCS#7
     * (The right way is to check Form field)
     *
     * @return bool
     */
    public function isSignature()
    {
        if (!$this->data) {
            $this->fillData();
        }
        if ($this->data && preg_match('~adbe\.pkcs7\.detached~', $this->data, $match)) {
            return true;
        }

        return false;
    }

    /**
     * Re-calculate digest and compare it to saved in document
     *
     * @return bool
     */
    public function isValidSignature()
    {
        if (!$this->data) {
            $this->fillData();
        }
        if (preg_match('~ByteRange \[(\d+) (\d+) (\d+) (\d+)\]~', $this->data, $mm)) {
            fseek($this->fp, $mm[1]);
            $buf = fread($this->fp, $mm[2]);
            fseek($this->fp, $mm[3]);
            $len = $mm[3] - $mm[2];
            $buf .= '<' . str_repeat('0', $len - 2) . '>';
            $tail = fread($this->fp, $mm[4]);
            $emptyRanges = 'ByteRange [0000000000 0000000000 0000000000 0000000000]';
            if (preg_match('~ByteRange \[(\d+) (\d+) (\d+) (\d+)\]~', $tail, $mm, PREG_OFFSET_CAPTURE)) {
                $tail = substr($tail, 0, $mm[0][1]) . $emptyRanges . substr($tail, $mm[0][1] + strlen($emptyRanges));
            }
            $hash = '';
            if (preg_match('~\/ID \[(.*)\]~', $tail, $mm)) {
                $hash = str_replace(['<', '>'], '', $mm[1]);
            }
            if (($pos = strpos($tail, 'xref')) !== false) {
                $buf .= substr($tail, 0, $pos);
                return $hash == hash('sha256', $buf);
            }
        }

        return false;
    }

    public function __call($name, $args)
    {
        if (substr($name, 0, 3) == 'get') {
            $var = strtolower(substr($name, 3));
            if (isset($this->{$var})) {
                return $this->{$var};
            }
        }
    }

    public function fillData()
    {
        if (!$this->data) {
            fseek($this->fp, $this->start);
            $this->data = '';
            do {
                $line = fgets($this->fp);
                $this->data .= $line;
            } while (substr(trim($line), -6) != 'endobj' || feof($this->fp));
        }
    }

    /**
     * Signature file can be viewed with openssl:
     *
     * $ openssl pkcs7 -in signature.bin -inform der -print
     *
     * Additional option -print_certs will display only public certificates
     *
     */
    public function parseSignature()
    {
        if (!$this->data) {
            $this->fillData();
        }
        $parts = explode('/', $this->data);
        foreach ($parts as $part) {
            if (preg_match('~Contents\s?<(.*)>~', $part, $mm)) {
                $this->tempSignName = tempnam(sys_get_temp_dir(), 'pdfcheck_');
                $this->tempCertName = tempnam(sys_get_temp_dir(), 'pdfcheck_');
                // save signature to temp
                $this->createTempSignatureFile($mm[1]);
                // save certificate to temp
                $this->createTempCertFile();
                break;
            }
        }
    }

    public function getCertInfo()
    {
        return $this->engine->getCertInfo($this->tempCertName);
    }

    public function verifyCertificate($caFile, $crlFile)
    {
        return $this->engine->verifyCertificate($this->tempCertName, $caFile, $crlFile);
    }

    public function createTempSignatureFile($data)
    {
        $fp = fopen($this->tempSignName, 'wb');
        // primitive ASN.1 parser
        if (substr($data, 0, 4) == '3082') {
            // DER format
            $counter = base_convert(substr($data, 4, 4), 16, 10);
            for ($i = 0; $i < $counter * 2 + 8; $i += 2) {
                $str = substr($data, $i, 2);
                $buf = hex2bin($str);
                fwrite($fp, $buf, sizeof($buf));
            }
        }
        fclose($fp);
    }

    /**
     * Save signature to DER file
     *
     * @param $signFile
     * @param int $suffix
     * @return string
     */
    public function saveSignFile($signFile, $suffix = 0)
    {
        $message = '';
        if ($suffix) {
            $parts = pathinfo($signFile);
            $signFile = $parts['dirname'] . '/' . $parts['filename'] . $suffix . '.' . ($parts['extension'] ?? 'pem');
        }
        if (copy($this->tempSignName, $signFile)) {
            $message = 'Signature in DER format was saved to ' . realpath($signFile);
        }

        return $message;
    }

    public function createTempCertFile()
    {
        $this->engine->createTempCertFile($this->tempCertName, $this->tempSignName);
    }

    /**
     * Save certificate to PEM file
     *
     * @param $certFile
     * @param int $suffix
     * @return string
     */
    public function saveCertFile($certFile, $suffix = 0)
    {
        $message = '';
        if ($suffix) {
            $parts = pathinfo($certFile);
            $certFile = $parts['dirname'] . '/' . $parts['filename'] . $suffix . '.' . ($parts['extension'] ?? 'pem');
        }
        if (copy($this->tempCertName, $certFile)) {
            $message = 'Certificate in PEM format was saved to ' . realpath($certFile);
        }

        return $message;
    }

    /**
     * Parse Info block
     *
     * @return array
     */
    public function parseInfo()
    {
        if (!$this->data) {
            $this->fillData();
        }
        $info = [];
        if (preg_match('~<<(.*)>>~s', $this->data, $match)) {
            $parts = explode("\n", $match[1]);
            foreach ($parts as $line) {
                if (preg_match('~/(\w+)\((.*)\)~', $line, $mm)) {
                    $info[$mm[1]] = $this->getNonEncoded($mm[2]);
                }
                if (preg_match('~/(\w+)\<(.*)\>~', $line, $mm)) {
                    $info[$mm[1]] = $this->getEncoded($mm[2]);
                }
            }
        }

        return $info;
    }

    /**
     * Parse non encoded string
     *
     * Example 1 - non encoded string:  GPL Ghostscript 9.20
     * Example 2 - encoded date string: D:20171112183546+02'00'
     *
     * @param string $str
     * @return string
     */
    protected function getNonEncoded($str)
    {
        if (preg_match('~D:(.*)\'~', $str, $mm)) {
            return date('r', strtotime(str_replace("'", ':', $mm[1])));
        } else {
            return $str;
        }
    }

    /**
     * Parse hex encoded string
     *
     * Example: FEFF0044007200610077
     *
     * @param string $str
     * @return string
     */
    protected function getEncoded($str)
    {
        $result = $str;
        if (substr($str, 0, 4) == 'FEFF') {
            $result = '';
            for ($i = 4; $i < strlen($str); $i += 4) {
                $result .= chr(hexdec(substr($str, $i, 4)));
            }
        }

        return $result;
    }
}
