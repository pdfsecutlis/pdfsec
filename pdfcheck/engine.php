<?php
/**
 * Created by PhpStorm.
 * User: main
 * Date: 12/20/17
 * Time: 9:44 PM
 */

namespace PDFReader;

interface Engine
{
    public function createTempCertFile($certFile, $signFile);
    public function verifyCertificate($certFile, $caFile, $crlFile);
    public function getCertInfo($certFile);
}