<?php
/**
 * Created by PhpStorm.
 * User: main
 * Date: 12/20/17
 * Time: 9:44 PM
 */

namespace PDFReader;

class Openssl implements Engine
{
    public function createTempCertFile($certFile, $signFile)
    {
        $fp = fopen($certFile, 'w');
        exec('openssl pkcs7 -in ' . $signFile . ' -inform der -print_certs', $output);
        $in = false;
        foreach ($output as $string) {
            if (!$in && strstr($string, 'BEGIN CERTIFICATE')) {
                $in = true;
            }
            if ($in && strlen($string)) {
                fputs($fp, $string . "\n");
            }
            if ($in && strstr($string, 'END CERTIFICATE')) {
                $in = false;
            }
        }
        fclose($fp);
    }

    public function verifyCertificate($certFile, $caFile, $crlFile)
    {
        exec('openssl verify -CAfile ' . $caFile . ' -crl_check -CRLfile ' . $crlFile .' ' . $certFile . ' >/dev/null 2>&1', $output, $res);

        return $res;
    }

    public function getCertInfo($certFile)
    {
        exec('openssl x509 -in ' . $certFile . ' -subject -issuer -dates -email -noout', $output);
        $message = '';
        foreach ($output as $string) {
            if (strlen($string)) {
                if (($pos = strpos($string, '=')) !== false) {
                    $title = substr($string, 0, $pos + 1);
                    $value = substr($string, $pos + 1);
                    $message .= '  ' . str_pad(ucfirst(substr($title, 0, -1)) . ':', INFO_COLUMN_WIDTH) . $value . "\n";
                }
            }
        }

        return $message;
    }

}