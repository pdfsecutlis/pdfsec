<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Zabara <wlady2001@gmail.com>
 * Date: 11/23/17
 * Time: 11:48 AM
 */

namespace PDFReader;

class PDF
{
    const MIN_TRAILER_BUF_SIZE = 1024;
    const MAX_PDF_VERSION = 1.7;
    public $engine = null;
    // file pointer
    public $fp = null;
    protected $elements = null;
    protected $lastObjOffset = 0;
    protected $info = [];
    protected $infoElement = 0;

    public function __construct($filename, Engine $engine)
    {
        if (!file_exists($filename)) {
            throw new \Exception('File was not found ' . $filename);
        }
        if (!($this->fp = fopen($filename, 'rb'))) {
            throw new \Exception('Error reading ' . $filename);
        }
        // detect PDF version
        $data = trim(fgets($this->fp));
        list($version) = sscanf($data, "%%PDF-%f");
        if (!intval($version)) {
            throw new \Exception($filename . ' is not a PDF file');
        }
        if ($version > self::MAX_PDF_VERSION) {
            throw new \Exception('Max PDF version is ' . self::MAX_PDF_VERSION);
        }
        $this->engine = $engine;
    }

    /**
     * Read linked xref tables
     *
     * @throws \Exception
     */
    public function initElements()
    {
        $fpos = 0;
        $fstat = fstat($this->fp);
        if ($fstat['size']>self::MIN_TRAILER_BUF_SIZE) {
            // read from the end
            fseek($this->fp, -self::MIN_TRAILER_BUF_SIZE, SEEK_END);
            $fpos = ftell($this->fp);
        }
        $data = fread($this->fp, self::MIN_TRAILER_BUF_SIZE);
        $pos = strpos($data, 'startxref');
        $xrefList = [];
        if ($pos !== false) {
            $offs = $fpos + $pos;
            fseek($this->fp, $offs);
            // skip startxref line
            fgets($this->fp);
            // get offset
            $line = fgets($this->fp);
            $offs = intval(trim($line));
            do {
                $data = $this->getXref($offs);
                array_unshift($xrefList, $data);
                $offs = $this->getPrevXref();
            } while ($offs !== false);
            // build full xref table
            $this->elements = [];
            foreach ($xrefList as $xref) {
                $lines = explode("\n", $xref);
                $index = 0;
                foreach ($lines as $line) {
                    list($start, $rev, $type) = sscanf($line, "%d %d %c");
                    if (!$start && !$rev && !$type) {
                        continue;
                    }
                    if (isset($type)) {
                        $this->elements[$index] = new Element($this, $start, $rev, $index);
                        $index++;
                    } else if (is_numeric($start) && is_numeric($rev)) {
                        $index = $start;
                    }
                }
            }
        } else {
            throw new \Exception('Section startxref was not found');
        }
    }

    public function getElements()
    {
        if (!$this->elements) {
            $this->initElements();
        }

        return $this->elements;
    }

    /**
     * Get PDF document info
     *
     * @return array
     */
    public function getInfo()
    {
        if (!$this->elements) {
            $this->initElements();
        }
        $object = $this->elements[$this->infoElement];
        if ($object instanceof Element) {
            return $object->parseInfo();
        }
    }

    /**
     * Get xref table
     *
     * @param $offs
     * @return string
     */
    protected function getXref($offs)
    {
        fseek($this->fp, $offs);
        $data = '';
        do {
            $line = fgets($this->fp);
            $data .= $line;
            if (strpos($line, 'trailer') !== false) {
                break;
            }
        } while (!feof($this->fp));

        return $data;
    }

    /**
     * Return the offset to previous xref table.
     * Besides it saves the number of Info block.
     *
     * @return bool|int False if not found
     */
    protected function getPrevXref()
    {
        $data = '';
        do {
            $line = fgets($this->fp);
            $data .= $line;
            if (trim($line) == '>>') {
                break;
            }
        } while (!feof($this->fp));
        $trailer = explode('/', $data);
        $prev = false;
        foreach ($trailer as $line) {
            if (preg_match('~(\w+) (.*)~', $line, $match)) {
                if ($match[1] == 'Prev') {
                    $prev = intval($match[2]);
                } else if ($match[1] == 'Info') {
                    list($el, $rev, $type) = sscanf($match[2], "%d %d %c");
                    $this->infoElement = intval($el);
                }
            }
        }

        return $prev;
    }
}
