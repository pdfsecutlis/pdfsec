<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Zabara <wlady2001@gmail.com>
 * Date: 11/23/17
 * Time: 10:12 AM
 */

namespace PDFReader;

define('INFO_COLUMN_WIDTH', 20);

$usage = <<<EOU
Usage: $argv[0] options

Options are:
 --in source      Input PDF file
 --sign file      Save signatures to file in DER format
 --cert file      Save certificates to file in PEM format
 --ca source      A source of trusted certificates. Used only with --crl option
 --crl source     Source of CRL's (in PEM format). Used only with --ca option
 -i               Get PDF info
 -s               Find signature
 -c               Get certificate info
 -q               Quiet mode 
 -h               This help info

Returned codes:

0 - OK
1 - usage error
2 - signature was not found
3 - signature is wrong
4 - certificate is wrong

In "quiet mode" you can check return code by running:

> echo $?

EOU;

$options = getopt('imschq', ['in:', 'cert:', 'sign:', 'ca:', 'crl:']);

$retCode = 0;

$quietMode = isset($options['q']);

if ((isset($options['ca']) && !isset($options['crl'])) ||
    (isset($options['crl']) && !isset($options['ca'])) ||
    isset($options['h']) || !isset($options['in'])
) {
    msg($usage);
    exit($retCode = 1);
}

try {
    $pdf = new PDF($options['in'], new Openssl());
    $foundInfo = false;
    $foundSign = false;
    $counter = 0;
    foreach ($pdf->getElements() as $element) {
        if (!$quietMode) {
            // in quiet mode this block will be ignored completely
            if (isset($options['i']) && !$foundInfo) {
                $info = $pdf->getInfo();
                if (count($info)) {
                    msg('Document info');
                    foreach ($info as $key => $value) {
                        if ($key) {
                            msg('  ' . str_pad($key . ':', INFO_COLUMN_WIDTH) . $value);
                        }
                    }
                } else {
                    msg('Document info block was not found');
                }
                $foundInfo = true;
            }
        }
        /** @var Element $element */
        if ($element->isSignature()) {
            $foundSign = true;
            $counter++;
            $element->parseSignature();
            if (isset($options['s'])) {
                if ($element->isValidSignature()) {
                    $message = 'Valid';
                } else {
                    $message = 'Digest Mismatch';
                    $retCode = 3;
                }
                msg("Signature #$counter: $message");
            }
            if (!empty($options['sign'])) {
                $message = $element->saveSignFile($options['sign'], $counter);
                msg($message);
            }
            if (isset($options['c'])) {
                $message = $element->getCertInfo();
                msg("Certificate #$counter Info:\n$message");
            }
            if (isset($options['ca']) && isset($options['crl'])) {
                $res = $element->verifyCertificate($options['ca'], $options['crl']);
                if (!$res) {
                    $message = 'OK';
                } else {
                    switch ($res) {
                        case 1:
                            $message = 'A file of trusted certificates was not found';
                            break;
                        case 2:
                        default:
                            $message = 'Certificate is wrong';
                            break;
                    }
                    $retCode = 4;
                }
                msg("Certificate #$counter: $message");
            }
            if (!empty($options['cert'])) {
                $message = $element->saveCertFile($options['cert'], $counter);
                msg($message);
            }
        }
    }
    if (!$foundSign && (isset($options['s']) || isset($options['c']))) {
        msg('Signature was not found');
        $retCode = 2;
    }
    msg('');
} catch (\Exception $e) {
    msg($e->getMessage());
    exit($retCode = 1);
}

exit($retCode);

function msg($message)
{
    global $quietMode;

    if (!$quietMode) {
        echo $message . "\n";
    }
}